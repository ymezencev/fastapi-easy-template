from fastapi import FastAPI
from fastapi_sqlalchemy import DBSessionMiddleware

from src.core.middleware import ExceptionMiddleware
from src.database.core import engine


def init_middleware(app: FastAPI) -> None:
    """Initialize all middlewares in application."""
    app.add_middleware(DBSessionMiddleware, custom_engine=engine, commit_on_exit=True)
    app.add_middleware(ExceptionMiddleware)
