import os

from starlette.config import Config


config = Config("../.env")

DEBUG = config("DEBUG", cast=bool, default=True)
TLS = config("TLS", default="off")
SERVER_HOST = config("SERVER_HOST", default="localhost")
SERVER_PORT = config("SERVER_PORT", cast=int, default="8000")
SITE_URL = f"https://{SERVER_HOST}" if TLS == "on" else f"http://{SERVER_HOST}:{SERVER_PORT}"
ENVIRONMENT = config("ENVIRONMENT", default="development")  # development/production
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# auth
JWT_SECRET = config("JWT_SECRET", default=None)
JWT_ALG = config("JWT_ALG", default="HS256")
JWT_EXP = config("JWT_EXP", cast=int, default=86400)

# database
POSTGRES_HOST = config("POSTGRES_HOST", default="localhost")
POSTGRES_PORT = config("POSTGRES_PORT", cast=int, default=5432)
POSTGRES_USER = config("POSTGRES_USER", default="postgres")
POSTGRES_PASSWORD = config("POSTGRES_PASSWORD", default="postgres")
POSTGRES_DATABASE = config("POSTGRES_DATABASE", default="postgres")
DATABASE_ENGINE_POOL_SIZE = config("DB_ENGINE_POOL_SIZE", cast=int, default=20)
DATABASE_ENGINE_MAX_OVERFLOW = config("DB_ENGINE_MAX_OVERFLOW", cast=int, default=0)
DB_AUTOFLUSH = False
DB_AUTOCOMMIT = False
DB_EXPIRE_ON_COMMIT = False
SQLALCHEMY_DATABASE_URI = (
    f"postgresql+psycopg2://"
    f"{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}:{POSTGRES_PORT}/{POSTGRES_DATABASE}"
)


# logger
LOG_LEVEL = config("LOG_LEVEL", default="WARNING")
JSON_LOGS = config("JSON_LOGS", cast=bool, default=False)
