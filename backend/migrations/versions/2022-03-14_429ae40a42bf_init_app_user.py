"""init app user

Revision ID: 429ae40a42bf
Revises:
Create Date: 2022-03-14 23:21:13.114152

"""
import sqlalchemy as sa
from alembic import op


# revision identifiers, used by Alembic.
revision = '429ae40a42bf'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'app_user',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('email', sa.String(length=128), nullable=False),
        sa.Column('role', sa.String(length=15), nullable=False),
        sa.Column('first_name', sa.String(length=128), nullable=True),
        sa.Column('last_name', sa.String(length=128), nullable=True),
        sa.Column('middle_name', sa.String(length=128), nullable=True),
        sa.Column('password', sa.LargeBinary(), nullable=False),
        sa.Column('is_active', sa.Boolean(), nullable=False),
        sa.Column('is_superuser', sa.Boolean(), nullable=False),
        sa.Column('is_verified', sa.Boolean(), nullable=False),
        sa.Column('last_login', sa.DateTime(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=True),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('id', name=op.f('pk__app_user')),
        sa.UniqueConstraint('email', name=op.f('uq__app_user__email')),
    )
    op.create_index(op.f('ix__app_user__role'), 'app_user', ['role'], unique=False)


def downgrade():
    op.drop_index(op.f('ix__app_user__role'), table_name='app_user')
    op.drop_table('app_user')
