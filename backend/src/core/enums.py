from enum import Enum, auto


class StrEnum(str, Enum):
    """
    StrEnum subclasses that create variants using `auto()` will have values equal to their names.

    Enums inheriting from this class that set values using `enum.auto()`
    will have variant values equal to their names
    """

    def _generate_next_value_(name, start, count, last_values) -> str:  # type: ignore
        return name


class UserRoles(StrEnum):
    superuser = auto()
    admin = auto()
    director = auto()
    manager = auto()
    worker = auto()
