import re
import uuid
from typing import Any, Optional, Union

from pydantic import BaseModel, ValidationError
from pydantic.error_wrappers import ErrorWrapper
from sqlalchemy import MetaData, create_engine, inspect
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import as_declarative

from config import settings
from src.core.exceptions import NotFoundError


engine = create_engine(
    settings.SQLALCHEMY_DATABASE_URI,
    pool_size=settings.DATABASE_ENGINE_POOL_SIZE,
    max_overflow=settings.DATABASE_ENGINE_MAX_OVERFLOW,
    future=True,
    echo=True,
)


convention: dict[str, Any] = {
    "all_column_names": lambda constraint, table: "_".join(
        [column.name for column in constraint.columns.values()]
    ),
    "ix": "ix__%(table_name)s__%(all_column_names)s",
    "uq": "uq__%(table_name)s__%(all_column_names)s",
    "ck": "ck__%(table_name)s__%(constraint_name)s",
    "fk": "fk__%(table_name)s__%(all_column_names)s__" "%(referred_table_name)s",
    "pk": "pk__%(table_name)s",
}

metadata = MetaData(naming_convention=convention)


def resolve_table_name(name: str) -> str:
    """Resolve table names to their mapped names."""
    names = re.split("(?=[A-Z])", name)
    return "_".join([x.lower() for x in names if x])


@as_declarative(metadata=metadata)
class Base:
    id: Union[int, uuid.UUID]
    __name__: str
    __repr_attrs__: list[str] = []
    __repr_max_length__: int = 15

    @declared_attr
    def __tablename__(self) -> str:
        return resolve_table_name(self.__name__)

    def dict(self) -> dict[str, str]:
        """Return a dict representation of a model."""
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}  # type: ignore

    @property
    def _id_str(self) -> str:
        ids = inspect(self).identity
        if ids:
            return "-".join([str(x) for x in ids]) if len(ids) > 1 else str(ids[0])
        else:
            return "None"

    @property
    def _repr_attrs_str(self) -> str:
        max_length = self.__repr_max_length__

        values = []
        single = len(self.__repr_attrs__) == 1
        for key in self.__repr_attrs__:
            if not hasattr(self, key):
                raise KeyError(
                    "{} has incorrect attribute '{}' in "
                    "__repr__attrs__".format(self.__class__, key)
                )
            value = getattr(self, key)
            wrap_in_quote = isinstance(value, str)

            value = str(value)
            if len(value) > max_length:
                value = value[:max_length] + "..."

            if wrap_in_quote:
                value = "'{}'".format(value)
            values.append(value if single else "{}:{}".format(key, value))

        return " ".join(values)

    def __repr__(self) -> str:
        id_str = ("#" + self._id_str) if self._id_str else ""
        repr_attrs = " " + self._repr_attrs_str if self._repr_attrs_str else ""
        return f"{self.__class__.__name__} {id_str}{repr_attrs}"


def get_model_name_by_tablename(table_fullname: str) -> str:
    """Return the model name of a given table."""
    return get_class_by_tablename(table_fullname=table_fullname).__name__


def get_class_by_tablename(table_fullname: str) -> Any:
    """Return class reference mapped to table."""

    def _find_class(name: str) -> Optional[Base]:
        for c in Base.registry._class_registry.values():  # type: ignore
            if hasattr(c, "__table__"):
                if c.__table__.fullname.lower() == name.lower():
                    return c
        return None

    mapped_name = resolve_table_name(table_fullname)
    mapped_class = _find_class(mapped_name)

    if not mapped_class:
        raise ValidationError(
            [
                ErrorWrapper(
                    NotFoundError(msg="Model not found. Check the name of your model."),
                    loc="filter",
                )
            ],
            model=BaseModel,
        )

    return mapped_class
