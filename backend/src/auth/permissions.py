from abc import ABC, abstractmethod
from typing import Optional

from fastapi import HTTPException
from starlette.requests import Request
from starlette.status import HTTP_403_FORBIDDEN

from src.auth.service import get_current_user
from src.core.enums import UserRoles


def any_permission(permissions: list, request: Request) -> bool:
    for p in permissions:
        try:
            p(request=request)
            return True
        except HTTPException:
            pass
    return False


class BasePermission(ABC):
    """
    Abstract permission that all other Permissions must be inherited from.

    Defines basic error message, status & error codes.

    Upon initialization, calls abstract method  `has_required_permissions`
    which will be specific to concrete implementation of Permission class.

    You would write your permissions like this:

    .. code-block:: python

        class TeapotUserAgentPermission(BasePermission):

            def has_required_permissions(self, request: Request) -> bool:
                return request.headers.get('User-Agent') == "Teapot v1.0"

    """

    error_msg = [{"msg": "You don't have permission to access or modify this resource."}]
    status_code = HTTP_403_FORBIDDEN
    role: Optional[str] = None

    @abstractmethod
    def has_required_permissions(self, request: Request) -> bool:
        ...

    def __init__(self, request: Request):
        self.user = get_current_user(request=request)
        if not self.user:
            raise HTTPException(status_code=self.status_code, detail=self.error_msg)
        self.role = self.user.role
        if not self.has_required_permissions(request):
            raise HTTPException(status_code=self.status_code, detail=self.error_msg)


class PermissionsDependency(object):
    """
    Define and check all the permission classes from one place inside route definition.

    Use it as an argument to FastAPI's `Depends` as follows:

    .. code-block:: python

        app = FastAPI()

        @app.get(
            "/teapot/",
            dependencies=[Depends(
                PermissionsDependency([TeapotUserAgentPermission]))]
        )
        async def teapot() -> dict:
            return {"teapot": True}
    """

    def __init__(self, permissions_classes: list):
        self.permissions_classes = permissions_classes

    def __call__(self, request: Request) -> None:
        for permission_class in self.permissions_classes:
            permission_class(request=request)


class SuperuserPermission(BasePermission):
    def has_required_permissions(self, request: Request) -> bool:
        if self.role == UserRoles.superuser:
            return True
        return False


class AdminPermission(BasePermission):
    def has_required_permissions(self, request: Request) -> bool:
        permissions = [SuperuserPermission]
        if self.role == UserRoles.admin or any_permission(permissions, request):
            return True
        return False


class DirectorPermission(BasePermission):
    def has_required_permissions(self, request: Request) -> bool:
        permissions = [AdminPermission]
        if self.role == UserRoles.director or any_permission(permissions, request):
            return True
        return False


class ManagerPermission(BasePermission):
    def has_required_permissions(self, request: Request) -> bool:
        permissions = [DirectorPermission]
        if self.role == UserRoles.manager or any_permission(permissions, request):
            return True
        return False


class WorkerPermission(BasePermission):
    def has_required_permissions(self, request: Request) -> bool:
        permissions = [ManagerPermission]
        if self.role == UserRoles.worker or any_permission(permissions, request):
            return True
        return False
