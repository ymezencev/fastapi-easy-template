from datetime import datetime

from fastapi import APIRouter, Depends, HTTPException, status
from pydantic.error_wrappers import ErrorWrapper, ValidationError

from src.auth import service as user_service
from src.auth.models import (
    AppUser,
    UserLogin,
    UserLoginResponse,
    UserRead,
    UserRegister,
    UserRegisterResponse,
    UserUpdate,
)
from src.auth.permissions import PermissionsDependency, SuperuserPermission
from src.core.exceptions import (
    InvalidConfigurationError,
    InvalidPasswordError,
    InvalidUsernameError,
)


user_router = APIRouter()
auth_router = APIRouter()


@user_router.get("")
def get_users() -> dict:
    return {"success": True}


@user_router.get("/{user_id}", response_model=UserRead)
def get_user(user_id: int) -> AppUser:
    """Get a user by id."""
    user = user_service.get(user_id)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=[{"msg": "The user with this id does not exist."}],
        )
    return user


@auth_router.get("/me", response_model=UserRead)
def get_me(current_user: AppUser = Depends(user_service.get_current_user)) -> AppUser:
    """Get a current logged in user."""
    return current_user


@user_router.patch("/me", response_model=UserRead)
def update_current_user(
    user_in: UserUpdate, current_user: AppUser = Depends(user_service.get_current_user)
) -> AppUser:
    """Update current user data."""
    return user_service.update_user(current_user, user_in)


@user_router.patch(
    "/{user_id}",
    response_model=UserRead,
    dependencies=[Depends(PermissionsDependency([SuperuserPermission]))],
)
def update_user(user_id: int, user_in: UserUpdate) -> AppUser:
    """Update a user data. Only available for superusers."""
    user = user_service.get(user_id)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=[{"msg": "The user with this id does not exist."}],
        )
    return user_service.update_user(user, user_in)


@auth_router.post("/login", response_model=UserLoginResponse)
def login_user(user_in: UserLogin) -> UserLoginResponse:
    """Login a user by email and password."""
    user = user_service.get_by_email(user_in.email)
    if user and user.check_password(user_in.password):
        user = user_service.update_user(user, UserUpdate(last_login=datetime.utcnow()))
        return UserLoginResponse(token=user.token)

    raise ValidationError(
        [
            ErrorWrapper(
                InvalidUsernameError(msg="Invalid username."),
                loc="username",
            ),
            ErrorWrapper(
                InvalidPasswordError(msg="Invalid password."),
                loc="password",
            ),
        ],
        model=UserLogin,
    )


@auth_router.post("/register", response_model=UserRegisterResponse)
def register_user(user_in: UserRegister) -> UserRegisterResponse:
    """Register a new user by email and password."""
    user = user_service.get_by_email(user_in.email)
    if user:
        raise ValidationError(
            [
                ErrorWrapper(
                    InvalidConfigurationError(msg="A user with this email already exists."),
                    loc="email",
                )
            ],
            model=UserRegister,
        )

    user = user_service.create(user_in=user_in)
    return UserRegisterResponse(token=user.token)
