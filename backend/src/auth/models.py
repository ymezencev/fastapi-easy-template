import re
from datetime import datetime, timedelta
from typing import Any, Optional

import bcrypt
from jose import jwt
from pydantic import EmailStr, Field, root_validator, validator
from sqlalchemy import Column, Integer, String, func
from sqlalchemy.sql.sqltypes import Boolean, DateTime, LargeBinary

from config import settings
from src.core.enums import UserRoles
from src.core.models import BaseSchema, TimeStampMixin
from src.database.core import Base


def hash_password(password: str) -> bytes:
    """Generate a hashed version of the provided password."""
    pw = bytes(password, "utf-8")
    salt = bcrypt.gensalt()
    return bcrypt.hashpw(pw, salt)


def validate_password(password: str) -> Any:
    password_pattern = r"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"
    if re.fullmatch(password_pattern, password) is None:
        raise ValueError("Password doesnt meet the conditions.")
    return password


# SQLAlchemy models...
class AppUser(Base, TimeStampMixin):

    id = Column(Integer, primary_key=True)
    email = Column(String(128), unique=True, nullable=False)
    role = Column(String(15), default=UserRoles.worker, index=True, nullable=False)
    first_name = Column(String(128), nullable=True)
    last_name = Column(String(128), nullable=True)
    middle_name = Column(String(128), nullable=True)

    password = Column(LargeBinary, nullable=False)
    is_active = Column(Boolean, default=True, nullable=False)
    is_superuser = Column(Boolean, default=False, nullable=False)
    is_verified = Column(Boolean, default=False, nullable=False)
    last_login = Column(DateTime, default=func.now())

    @property
    def fio(self) -> str:
        return f"{self.last_name} {self.first_name} {self.middle_name}"

    def check_password(self, password: str) -> bool:
        return bcrypt.checkpw(password.encode("utf-8"), self.password)  # type: ignore

    @property
    def token(self) -> str:
        now = datetime.utcnow()
        exp = (now + timedelta(seconds=settings.JWT_EXP)).timestamp()
        data = {
            "exp": exp,
            "id": self.id,
            "email": self.email,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "middle_name": self.middle_name,
            "role": self.role,
        }
        return jwt.encode(data, settings.JWT_SECRET, algorithm=settings.JWT_ALG)


# Pydantic models...
class UserBase(BaseSchema):
    email: EmailStr

    @validator("email")
    def email_required(cls, v: Any) -> EmailStr:
        if not v:
            raise ValueError("Must not be empty string and must be a email")
        return v


class UserLogin(UserBase):
    password: str

    @validator("password", pre=True, always=True)
    def validate_password(cls, v: Any) -> None:
        validate_password(password=v)
        return v


class UserRegister(UserLogin):
    @root_validator
    def modify_values(cls, values: dict[str, Any]) -> dict[str, Any]:
        if "password" in values:
            values["password"] = hash_password(str(values["password"]))
        return values


class UserLoginResponse(BaseSchema):
    token: Optional[str]


class UserRead(UserBase):
    id: int
    first_name: Optional[str]
    last_name: Optional[str]
    middle_name: Optional[str]
    role: str


class UserUpdate(BaseSchema):
    first_name: Optional[str]
    last_name: Optional[str]
    middle_name: Optional[str]
    password: Optional[str]
    password_confirmation: Optional[str]
    role: Optional[UserRoles]
    last_login: Optional[datetime]

    @validator("password", pre=True, always=True)
    def validate_password(cls, v: Any) -> None:
        if v is None:
            return
        return validate_password(password=v)

    @validator("password_confirmation", pre=True, always=True)
    def passwords_match(cls, v: Any, values: dict[str, Any], **kwargs: Any) -> Any:
        if "password" in values and v != values["password"]:
            raise ValueError("Passwords do not match")
        return v

    @root_validator
    def modify_values(cls, values: dict[str, Any]) -> dict[str, Any]:
        if "password" in values:
            values["password"] = hash_password(str(values["password"]))
            values["password_confirmation"] = None
        return values


class UserRegisterResponse(BaseSchema):
    token: Optional[str] = Field(None, nullable=True)


class UserPagination(BaseSchema):
    total: int
    items: list[UserRead] = []
