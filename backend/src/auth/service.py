from typing import Optional

from fastapi import HTTPException, status
from fastapi.security.utils import get_authorization_scheme_param
from fastapi_sqlalchemy import db
from jose import jwt
from jose.exceptions import JWKError, JWTError
from sqlalchemy import select, update
from starlette.requests import Request

from config.settings import JWT_SECRET
from src.auth.models import AppUser, UserRegister, UserUpdate


def get(user_id: int) -> Optional[AppUser]:
    """Return a user based on the given user id."""
    return db.session.execute(select(AppUser).where(AppUser.id == user_id)).scalars().one_or_none()


def get_by_email(email: str) -> Optional[AppUser]:
    """Return a user object based on user email."""
    return db.session.execute(select(AppUser).where(AppUser.email == email)).scalars().one_or_none()


def create(user_in: UserRegister) -> AppUser:
    """Create a new user."""
    create_data = user_in.dict()
    user = AppUser(**create_data)
    db.session.add(user)
    db.session.flush()
    return user


def update_user(user: AppUser, user_in: UserUpdate) -> AppUser:
    """Update a user."""
    update_data = user_in.dict(exclude_none=True)
    db.session.execute(update(AppUser).where(AppUser.id == user.id).values(**update_data))
    user = db.session.execute(select(AppUser).where(AppUser.id == user.id)).scalars().one()
    db.session.flush()
    return user


def get_or_create(user_in: UserRegister) -> AppUser:
    """Get an existing user or creates a new one."""
    user = get_by_email(user_in.email)
    if not user:
        user = create(user_in)
    return user


def get_current_user(request: Request) -> AppUser:
    """Attempt to get the current user depending on the configured authentication provider."""
    authorization: str = request.headers.get("Authorization")
    scheme, param = get_authorization_scheme_param(authorization)

    if not authorization or scheme.lower() != "bearer":
        msg = (
            f"Malformed authorization header. "
            f"Scheme: {scheme} Param: {param} Authorization: {authorization}"
        )
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail=[{"msg": msg}])

    token = authorization.split()[1]
    try:
        data = jwt.decode(token, JWT_SECRET)
    except (JWKError, JWTError) as e:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail=[{"msg": str(e)}])

    user = get(user_id=data["id"])
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=[{"msg": "Could not validate credentials"}],
        )
    return user
