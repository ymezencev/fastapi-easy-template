include .env
export
e=.env

up:  # Поднять контейнеры для локальной работы
	docker-compose $(f) --env-file $(e) up db

main.py:  # Запустить основное приложение
	python ./backend/main.py

beauty:  # запуск проверок перед коммитом
	pre-commit run --all-files
